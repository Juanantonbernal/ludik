export default {
    methods: {
        async callApi(method, url, dataObj) {
            try {
                return await axios({
                    method: method,
                    url: url,
                    data: dataObj
                });
            } catch (e) {
                return e.response
            }
        },
        async callGetApi(method, url, dataObj) {
            try {
                return await axios({
                    method: method,
                    url: url,
                    params: dataObj
                });
            } catch (e) {
                return e.response
            }
        },
        notification(type, message) {
            this.$notify({
                title: type,
                message: message,
                type: type
            });
        }
    },
}