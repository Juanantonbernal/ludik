<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partidas';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idJuego';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idJugador', 'idTicket', 'idTemporada', 'idcodigo', 'JuegoToken', 'estado', 'puntos', 'fechaInicio', 'fechaFin'
    ];

    /**
     * Get the usuario that owns the partida.
     */
    public function usuario()
    {
        // Modelo a relacionar, FK del Modelo User en la tbl Partida, id del Modelo user
        return $this->belongsTo('App\Models\User', 'idJugador', 'id');
    }
}
