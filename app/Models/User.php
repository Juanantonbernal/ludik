<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDisfraz', 'Nombre', 'Apellido', 'DNI', 'Celular', 'Correo', 'Fecha Nacimiento', 'Acepto', 'idProvincia', 'idDepartamento', 'fechaRegistro'
    ];

    /**
     * Get the partidas for the user
     */
    public function partidas()
    {
        // Modelo a relacionar y FK del modelo User en la tabla Partida
        return $this->hasMany('App\Models\Partida', 'idJugador');
    }

    public function scopeFecha($query, $f1, $f2)
    {
        if (($f1) && ($f2)) {
            return $query->whereDate('fechaRegistro', '>=', $f1)
                         ->whereDate('fechaRegistro', '<=', $f2);
        }
    }

    public function ratingSum()
    {
        return $this->hasMany('App\Models\Partida', 'idJugador')
            ->selectRaw('idJugador, SUM(puntos) as total')
            ->groupBy('idJugador');
    }

    public function difDates()
    {
        return $this->hasMany('App\Models\Partida', 'idJugador')
            ->selectRaw('idJugador, AVG(DATEDIFF(fechaFin, fechaInicio)) AS diferencia')
            ->groupBy('idJugador');
    }
}
