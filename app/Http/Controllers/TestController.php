<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function preguntaUno(Request $request)
    {
        $acepto = $request->acepto;

        $users = User::where('Acepto', $acepto)->withCount('partidas')->orderBy('partidas_count', 'desc')->get();

        return response()->json($users->take(10));
    }

    public function preguntaDos(Request $request)
    {
        $fi     =   $request->fechaInicio;
        $ff     =   $request->fechaFin;
        $letra  =   $request->letra;

        // Todos los usuarios
        $allUsers = User::count();
        // Usuarios filtrados por fecha
        $usersByDates = User::fecha($fi, $ff)->count();
        //Calculo de porcentaje
        $percent = $usersByDates / $allUsers * 100;

        // Usuarios fltrados por letra
        $usersByLeter = User::where('Nombre', 'like', $letra.'%')->get();

        return response()->json([
            'percent'       =>  round($percent, 2),
            'usersByLeter'  =>  $usersByLeter
        ]);
    }

    public function preguntaTres(Request $request)
    {
        $idDisfraz = $request->disfraz;

        // Obtener resultados
        $users = User::with('ratingSum')->where('idDisfraz', $idDisfraz)->get();

        // Setear resultados
        foreach ($users as $key => $value) {
            // return response()->json($value['ratingSum'][0]->total);
            $users[$key]['ratingSum'] = $value['ratingSum'][0]->total;
        }

        //Ordenar resultados
        $sorted = $users->sortBy([
            ['ratingSum', 'desc']
        ]);

        // Mostrar solo los 10
        $sorted->values()->take(10);

        return response()->json($sorted);
    }

    public function preguntaCuatro()
    {
        $users = User::with('difDates')->get();

        // Setear resultados
        foreach ($users as $key => $value) {
            // return response()->json($value['ratingSum'][0]->total);
            $users[$key]['difDates'] = $value['difDates'][0]->diferencia;
        }

        //Ordenar resultados
        $sorted = $users->sortBy([
            ['difDates', 'desc']
        ]);

        // Mostrar solo los 10
        $sorted->values()->take(10);

        return response()->json($sorted);
    }
}
