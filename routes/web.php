<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/preguntaUno', [TestController::class, 'preguntaUno']);
Route::get('/preguntaDos', [TestController::class, 'preguntaDos']);
Route::get('/preguntaTres', [TestController::class, 'preguntaTres']);
Route::get('/preguntaCuatro', [TestController::class, 'preguntaCuatro']);


Route::get('/{optional?}', function () {
    return view('welcome');
})->name('basepath')->where('optional', '.*');
